package com.oliver.hitbtctrader.data.repository;


import com.oliver.hitbtctrader.data.api.PublicApi;
import com.oliver.hitbtctrader.data.local.model.dao.SymbolsDAO;
import com.oliver.hitbtctrader.domain.SymbolsRepository;
import com.oliver.hitbtctrader.domain.model.Symbol;
import com.oliver.hitbtctrader.domain.test.SymbolTestData;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.reactivex.Flowable;

public class MockSymbolsRepositoryImpl implements SymbolsRepository {
    private final SymbolTestData mTestData;
    private Random r = new Random();

    public MockSymbolsRepositoryImpl(PublicApi api, SymbolsDAO localDao) {
        mTestData = new SymbolTestData();
    }

    @Override
    public Flowable<List<Symbol>> getSymbolsList() {
        return Flowable.just(prepareData());
    }

    private List<Symbol> prepareData() {
        int size = r.nextInt(20);
        List<Symbol> results = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            results.add(mTestData.getRandomSymbol());
        }
        return results;
    }
}
