package com.oliver.hitbtctrader.data.di;


import com.oliver.hitbtctrader.data.api.PublicApi;
import com.oliver.hitbtctrader.data.local.model.dao.SymbolsDAO;
import com.oliver.hitbtctrader.data.repository.MockSymbolsRepositoryImpl;
import com.oliver.hitbtctrader.data.repository.MockTickerRepositoryImpl;
import com.oliver.hitbtctrader.data.repository.MockTmpDataRepositoryImpl;
import com.oliver.hitbtctrader.domain.SymbolsRepository;
import com.oliver.hitbtctrader.domain.TickerRepository;
import com.oliver.hitbtctrader.domain.TmpDataRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    @Provides
    @Singleton
    public TmpDataRepository provideTmpDataRepository(PublicApi api) {
        return new MockTmpDataRepositoryImpl(api);
    }

    @Provides
    @Singleton
    public SymbolsRepository provideSymbolsRepository(PublicApi api, SymbolsDAO localDAO) {
        return new MockSymbolsRepositoryImpl(api, localDAO);
    }

    @Provides
    @Singleton
    public TickerRepository provideTickerRepository(PublicApi api) {
        return new MockTickerRepositoryImpl(api);
    }
}
