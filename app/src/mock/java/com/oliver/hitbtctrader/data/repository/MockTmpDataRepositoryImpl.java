package com.oliver.hitbtctrader.data.repository;


import com.oliver.hitbtctrader.data.api.PublicApi;
import com.oliver.hitbtctrader.domain.TmpDataRepository;
import com.oliver.hitbtctrader.domain.model.Time;

import io.reactivex.Observable;

public class MockTmpDataRepositoryImpl implements TmpDataRepository {


    public MockTmpDataRepositoryImpl(PublicApi api) {

    }

    @Override
    public Observable<Time> getTime() {
        return Observable.just(new Time(System.currentTimeMillis()));
    }
}
