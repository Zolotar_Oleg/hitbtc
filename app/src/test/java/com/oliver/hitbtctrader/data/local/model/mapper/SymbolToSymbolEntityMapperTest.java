package com.oliver.hitbtctrader.data.local.model.mapper;

import com.oliver.hitbtctrader.data.local.model.content.SymbolEntity;
import com.oliver.hitbtctrader.domain.model.Symbol;
import com.oliver.hitbtctrader.domain.test.SymbolTestData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.Assert.assertTrue;

@RunWith(JUnit4.class)
public class SymbolToSymbolEntityMapperTest {

    private SymbolToSymbolEntityMapper mMapper;
    private SymbolTestData mData;

    @Before
    public void setup() {
        mData = new SymbolTestData();
        mMapper = new SymbolToSymbolEntityMapper();
    }

    @Test
    public void testMapper() throws Exception {
        int index = mData.getRandomIndex();

        Symbol source = mData.getSymbolModel(index);
        SymbolEntity expected = mData.getSymbolEntity(index);

        SymbolEntity result = mMapper.apply(source);

        assertTrue("Mapper converting error", expected.equals(result));

    }
}
