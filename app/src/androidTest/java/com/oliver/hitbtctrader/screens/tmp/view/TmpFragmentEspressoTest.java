package com.oliver.hitbtctrader.screens.tmp.view;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.oliver.hitbtctrader.R;
import com.oliver.hitbtctrader.screens.main.view.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class TmpFragmentEspressoTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Before
    public void init() {
        mActivityRule.getActivity().showTimestamp();
    }
    @Test
    public void testLoadTimestampClick_() {
        // click on get Timestamp
        onView(withId(R.id.btn_get_timestamp)).perform(click());

        // check that text field isn't empty (timestamp or error)
        onView(withId(R.id.tv_timestamp)).check(matches(not(withText(""))));
    }
}
