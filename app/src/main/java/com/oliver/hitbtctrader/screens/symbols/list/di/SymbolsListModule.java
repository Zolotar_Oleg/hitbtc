package com.oliver.hitbtctrader.screens.symbols.list.di;

import com.oliver.hitbtctrader.domain.SymbolsRepository;
import com.oliver.hitbtctrader.screens.symbols.list.presenter.SymbolsListPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SymbolsListModule {

    @Provides
    @SymbolsListScope
    SymbolsListPresenter provideSymbolsListPresenter(SymbolsRepository repository) {
        return new SymbolsListPresenter(repository);
    }
}
