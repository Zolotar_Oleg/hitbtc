package com.oliver.hitbtctrader.screens.symbols.ticker.presenter;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.oliver.hitbtctrader.domain.TickerRepository;
import com.oliver.hitbtctrader.domain.model.Symbol;
import com.oliver.hitbtctrader.screens.symbols.ticker.view.SymbolTickerView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@InjectViewState
public class SymbolTickerPresenter extends MvpPresenter<SymbolTickerView> {

    private final TickerRepository mRepository;

    private Disposable mSymbolTickerDisposable;

    public SymbolTickerPresenter(TickerRepository repository) {
        mRepository = repository;
    }


    @Override
    public void detachView(SymbolTickerView view) {
        super.detachView(view);


        if (mSymbolTickerDisposable != null && !mSymbolTickerDisposable.isDisposed())
            mSymbolTickerDisposable.dispose();
    }

    public void loadSymbolTicker(Symbol symbol) {
        Timber.d("loadSymbolsList: ");

        mSymbolTickerDisposable = mRepository.getTicker(symbol.getSymbol())

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showLoadingDialog())
                .doAfterTerminate(getViewState()::dismissLoadingDialog)
                .subscribe(ticker -> {
                            Timber.d("loadSymbolTicker: list: " + ticker);
                            getViewState().showTicker(ticker);
                        },
                        throwable -> {
                            throwable.printStackTrace();
                            getViewState().showLoadingError();
                        });
    }

}
