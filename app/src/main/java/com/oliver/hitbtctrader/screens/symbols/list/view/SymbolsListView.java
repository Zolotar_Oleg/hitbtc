package com.oliver.hitbtctrader.screens.symbols.list.view;


import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.oliver.hitbtctrader.domain.model.Symbol;
import com.oliver.hitbtctrader.screens.base.LoadingView;

import java.util.List;

public interface SymbolsListView extends LoadingView {

    @StateStrategyType(SkipStrategy.class)
    void onFirstViewAttach();

    void showSymbolsList(List<Symbol> symbolList);

    void showLoadingError();
}
