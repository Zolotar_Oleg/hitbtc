package com.oliver.hitbtctrader.screens.base.di;

import com.oliver.hitbtctrader.screens.main.view.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract MainActivity bindMainActivity();

}
