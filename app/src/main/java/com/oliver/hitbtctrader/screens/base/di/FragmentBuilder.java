package com.oliver.hitbtctrader.screens.base.di;

import com.oliver.hitbtctrader.screens.symbols.list.di.SymbolsListModule;
import com.oliver.hitbtctrader.screens.symbols.list.di.SymbolsListScope;
import com.oliver.hitbtctrader.screens.symbols.list.view.SymbolsListFragment;
import com.oliver.hitbtctrader.screens.symbols.ticker.di.SymbolTickerModule;
import com.oliver.hitbtctrader.screens.symbols.ticker.di.SymbolTickerScope;
import com.oliver.hitbtctrader.screens.symbols.ticker.view.SymbolTickerFragment;
import com.oliver.hitbtctrader.screens.tmp.di.TmpModule;
import com.oliver.hitbtctrader.screens.tmp.di.TmpScope;
import com.oliver.hitbtctrader.screens.tmp.view.TmpFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuilder {

    @SymbolsListScope
    @ContributesAndroidInjector(modules = SymbolsListModule.class)
    abstract SymbolsListFragment provideSymbolsListFragment();

    @SymbolTickerScope
    @ContributesAndroidInjector(modules = SymbolTickerModule.class)
    abstract SymbolTickerFragment provideSymbolTickerFragment();

    @TmpScope
    @ContributesAndroidInjector(modules = TmpModule.class)
    abstract TmpFragment provideTmpFragment();
}
