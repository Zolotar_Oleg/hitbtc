package com.oliver.hitbtctrader.screens.tmp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.oliver.hitbtctrader.domain.TmpDataRepository;
import com.oliver.hitbtctrader.screens.tmp.view.TmpView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@InjectViewState
public class TmpPresenter extends MvpPresenter<TmpView> {
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss:SSS", Locale.US);
    private final TmpDataRepository mRepository;
    private Disposable mTimestampDisposable;

    public TmpPresenter(TmpDataRepository repository) {
        mRepository = repository;
    }

    @Override
    public void detachView(TmpView view) {
        super.detachView(view);

        if (mTimestampDisposable != null && !mTimestampDisposable.isDisposed())
            mTimestampDisposable.dispose();

    }


    public void loadTimestamp() {
        Timber.d( "loadTimestamp() called");
        mTimestampDisposable = mRepository.getTime()

                .map(time -> new Date(time.getTimestamp()))
                .map(mDateFormat::format)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showLoadingDialog())
                .doAfterTerminate(getViewState()::dismissLoadingDialog)
                .subscribe(getViewState()::showTimestamp,
                        throwable -> {
                            Timber.d("loadTimestamp: onError");
                            throwable.printStackTrace();
                            getViewState().showTimestampLoadingError();
                        },
                        () -> Timber.d("loadTimestamp: onCompleted"));
    }
}
