package com.oliver.hitbtctrader.screens.base;


import com.arellomobile.mvp.MvpView;

public interface LoadingView extends MvpView{

    void showLoadingDialog();

    void dismissLoadingDialog();
}
