package com.oliver.hitbtctrader.screens.symbols.list.presenter;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.oliver.hitbtctrader.domain.SymbolsRepository;
import com.oliver.hitbtctrader.screens.symbols.list.view.SymbolsListView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@InjectViewState
public class SymbolsListPresenter extends MvpPresenter<SymbolsListView> {

    private Disposable mSymbolsListDisposable;
    private SymbolsRepository mRepository;

    public SymbolsListPresenter(SymbolsRepository repository) {
        mRepository = repository;
    }


    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        // TODO: 10/08/17 not sure about this,
        getViewState().onFirstViewAttach();
    }

    @Override
    public void detachView(SymbolsListView view) {
        super.detachView(view);

        if (mSymbolsListDisposable != null && !mSymbolsListDisposable.isDisposed())
            mSymbolsListDisposable.dispose();
    }

    public void loadSymbolsList() {

        mSymbolsListDisposable = mRepository.getSymbolsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showLoadingDialog())
                .doAfterTerminate(() -> getViewState().dismissLoadingDialog())
                .subscribe(
                        symbolsList -> getViewState().showSymbolsList(symbolsList),
                        this::handleSymbolsListLoadingError);
    }

    private void handleSymbolsListLoadingError(Throwable throwable) {
        Timber.e(new RuntimeException("Error while loading symbols list", throwable));

        getViewState().showLoadingError();
    }

}
