package com.oliver.hitbtctrader.screens.tmp.di;

import com.oliver.hitbtctrader.domain.TmpDataRepository;
import com.oliver.hitbtctrader.screens.tmp.presenter.TmpPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class TmpModule {

    @TmpScope
    @Provides
    TmpPresenter provideTmpPresenter(TmpDataRepository tmpDataRepository) {
        return new TmpPresenter(tmpDataRepository);
    }
}
