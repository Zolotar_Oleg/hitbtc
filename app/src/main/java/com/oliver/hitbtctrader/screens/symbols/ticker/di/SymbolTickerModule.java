package com.oliver.hitbtctrader.screens.symbols.ticker.di;

import com.oliver.hitbtctrader.domain.TickerRepository;
import com.oliver.hitbtctrader.screens.symbols.ticker.presenter.SymbolTickerPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SymbolTickerModule {

    @SymbolTickerScope
    @Provides
    SymbolTickerPresenter provideSymbolTickerPresenter(TickerRepository tickerRepository) {
        return new SymbolTickerPresenter(tickerRepository);
    }
}
