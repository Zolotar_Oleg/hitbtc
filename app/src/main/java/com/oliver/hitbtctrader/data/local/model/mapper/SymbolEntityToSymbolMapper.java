package com.oliver.hitbtctrader.data.local.model.mapper;


import com.oliver.hitbtctrader.data.local.model.content.SymbolEntity;
import com.oliver.hitbtctrader.domain.model.Symbol;

import io.reactivex.functions.Function;


public class SymbolEntityToSymbolMapper implements Function<SymbolEntity, Symbol> {
    @Override
    public Symbol apply(SymbolEntity o) throws Exception {
        Symbol symbol = new Symbol();
        symbol.setSymbol(o.getSymbol());
        symbol.setStep(o.getStep());
        symbol.setLot(o.getLot());
        symbol.setCurrency(o.getCurrency());
        symbol.setCommodity(o.getCommodity());
        symbol.setTakeLiquidityRate(o.getTakeLiquidityRate());
        symbol.setProvideLiquidityRate(o.getProvideLiquidityRate());
        return symbol;
    }
}
