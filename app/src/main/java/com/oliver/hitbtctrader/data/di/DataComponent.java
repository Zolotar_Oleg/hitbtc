package com.oliver.hitbtctrader.data.di;

import android.content.Context;

import com.oliver.hitbtctrader.domain.SymbolsRepository;
import com.oliver.hitbtctrader.domain.TickerRepository;
import com.oliver.hitbtctrader.domain.TmpDataRepository;
import com.oliver.hitbtctrader.global.HitBTCApplication;
import com.oliver.hitbtctrader.global.di.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,

        NetworkModule.class,
        LocalDBModule.class,

        DataModule.class
})
public interface DataComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(HitBTCApplication application);

        DataComponent build();
    }

    Context getContext();

    TmpDataRepository getTmpDataRepository();

    SymbolsRepository getSymbolsRepository();

    TickerRepository getTickerRepository();
}
