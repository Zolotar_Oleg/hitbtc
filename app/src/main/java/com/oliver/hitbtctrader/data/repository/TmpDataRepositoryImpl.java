package com.oliver.hitbtctrader.data.repository;


import com.oliver.hitbtctrader.data.api.PublicApi;
import com.oliver.hitbtctrader.data.api.model.mapper.TimeApiModelToTimeMapper;
import com.oliver.hitbtctrader.domain.TmpDataRepository;
import com.oliver.hitbtctrader.domain.model.Time;

import io.reactivex.Observable;

public class TmpDataRepositoryImpl implements TmpDataRepository {

    private final PublicApi mApi;

    public TmpDataRepositoryImpl(PublicApi api) {
        mApi = api;
    }

    @Override
    public Observable<Time> getTime() {
        return mApi.getTimestamp()
                .map(new TimeApiModelToTimeMapper());
    }
}
