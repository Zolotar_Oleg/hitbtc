package com.oliver.hitbtctrader.data.local.model.mapper;


import com.oliver.hitbtctrader.data.local.model.content.SymbolEntity;
import com.oliver.hitbtctrader.domain.model.Symbol;

import io.reactivex.functions.Function;

public class SymbolToSymbolEntityMapper implements Function<Symbol, SymbolEntity> {

    @Override
    public SymbolEntity apply(Symbol o) throws Exception {
        SymbolEntity symbol = new SymbolEntity();
        symbol.setSymbol(o.getSymbol());
        symbol.setStep(o.getStep());
        symbol.setLot(o.getLot());
        symbol.setCurrency(o.getCurrency());
        symbol.setCommodity(o.getCommodity());
        symbol.setTakeLiquidityRate(o.getTakeLiquidityRate());
        symbol.setProvideLiquidityRate(o.getProvideLiquidityRate());
        return symbol;
    }
}
