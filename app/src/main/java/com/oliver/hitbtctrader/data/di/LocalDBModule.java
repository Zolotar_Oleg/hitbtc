package com.oliver.hitbtctrader.data.di;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.oliver.hitbtctrader.data.local.HitBTCDatabase;
import com.oliver.hitbtctrader.data.local.model.dao.SymbolsDAO;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LocalDBModule {

    @Provides
    @Singleton
    public HitBTCDatabase getDatabase(Context context){
        return Room.databaseBuilder(context,
                HitBTCDatabase.class, "hitbtc.db")
                .build();
    }

    @Provides
    @Singleton
    public SymbolsDAO getSymbolsDAO(HitBTCDatabase db){
        return db.symbolsDao();
    }

}
