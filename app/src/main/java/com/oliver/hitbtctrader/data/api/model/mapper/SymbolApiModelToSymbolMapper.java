package com.oliver.hitbtctrader.data.api.model.mapper;


import com.oliver.hitbtctrader.data.api.model.content.SymbolApiModel;
import com.oliver.hitbtctrader.domain.model.Symbol;

import io.reactivex.functions.Function;

public class SymbolApiModelToSymbolMapper implements Function<SymbolApiModel, Symbol> {

    @Override
    public Symbol apply(SymbolApiModel o) throws Exception {
        Symbol symbol = new Symbol();
        symbol.setSymbol(o.getSymbol());
        symbol.setStep(o.getStep());
        symbol.setLot(o.getLot());
        symbol.setCurrency(o.getCurrency());
        symbol.setCommodity(o.getCommodity());
        symbol.setTakeLiquidityRate(o.getTakeLiquidityRate());
        symbol.setProvideLiquidityRate(o.getProvideLiquidityRate());
        return symbol;
    }
}
