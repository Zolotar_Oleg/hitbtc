package com.oliver.hitbtctrader.data.api.model.responces;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.oliver.hitbtctrader.data.api.model.content.SymbolApiModel;

import java.util.List;

public class SymbolsListResponse {
    @SerializedName("symbols")
    @Expose
    private List<SymbolApiModel> symbols;

    public List<SymbolApiModel> getSymbols() {
        return symbols;
    }

    public void setSymbols(List<SymbolApiModel> symbols) {
        this.symbols = symbols;
    }
}
