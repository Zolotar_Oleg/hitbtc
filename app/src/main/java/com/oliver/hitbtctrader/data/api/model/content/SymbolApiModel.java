package com.oliver.hitbtctrader.data.api.model.content;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SymbolApiModel {

    @SerializedName("symbol")
    @Expose
    private String symbol;
    @SerializedName("step")
    @Expose
    private String step;
    @SerializedName("lot")
    @Expose
    private String lot;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("commodity")
    @Expose
    private String commodity;
    @SerializedName("takeLiquidityRate")
    @Expose
    private String takeLiquidityRate;
    @SerializedName("provideLiquidityRate")
    @Expose
    private String provideLiquidityRate;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getTakeLiquidityRate() {
        return takeLiquidityRate;
    }

    public void setTakeLiquidityRate(String takeLiquidityRate) {
        this.takeLiquidityRate = takeLiquidityRate;
    }

    public String getProvideLiquidityRate() {
        return provideLiquidityRate;
    }

    public void setProvideLiquidityRate(String provideLiquidityRate) {
        this.provideLiquidityRate = provideLiquidityRate;
    }

    @Override
    public String toString() {
        return "SymbolApiModel{" +
                "symbol='" + symbol + '\'' +
                ", step='" + step + '\'' +
                ", lot='" + lot + '\'' +
                ", currency='" + currency + '\'' +
                ", commodity='" + commodity + '\'' +
                ", takeLiquidityRate='" + takeLiquidityRate + '\'' +
                ", provideLiquidityRate='" + provideLiquidityRate + '\'' +
                '}';
    }

}
