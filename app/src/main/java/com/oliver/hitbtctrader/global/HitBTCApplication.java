package com.oliver.hitbtctrader.global;

import android.app.Application;
import android.app.Fragment;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.oliver.hitbtctrader.BuildConfig;
import com.oliver.hitbtctrader.data.di.DaggerDataComponent;
import com.oliver.hitbtctrader.data.di.DataComponent;
import com.oliver.hitbtctrader.global.di.AppComponent;
import com.oliver.hitbtctrader.global.di.DaggerAppComponent;
import com.oliver.hitbtctrader.global.logger.CrashlyticsLogTree;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasFragmentInjector;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * Created by oliver on 6/15/17.
 */

public class HitBTCApplication extends Application implements HasFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentInjector;

    @Override
    public void onCreate() {
        super.onCreate();

        initDagger();
        initCrashlytics();

        initTimber();
    }

    private void initDagger() {
        DataComponent dataComponent = DaggerDataComponent.builder()
                .application(this)
                .build();

        AppComponent appComponent = DaggerAppComponent.builder()
                .dataComponent(dataComponent)
//                .application(this)
                .build();

        appComponent.inject(this);
    }

    private void initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashlyticsLogTree());
        }
    }

    private void initCrashlytics() {
        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();

        Fabric.with(this, crashlyticsKit);
    }


    @Override
    public AndroidInjector<Fragment> fragmentInjector() {
        return fragmentInjector;
    }
}
