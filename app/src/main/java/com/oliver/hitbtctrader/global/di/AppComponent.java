package com.oliver.hitbtctrader.global.di;

import com.oliver.hitbtctrader.data.di.DataComponent;
import com.oliver.hitbtctrader.data.di.DataScope;
import com.oliver.hitbtctrader.global.HitBTCApplication;
import com.oliver.hitbtctrader.screens.base.di.ActivityBuilder;
import com.oliver.hitbtctrader.screens.base.di.FragmentBuilder;

import dagger.Component;
import dagger.android.AndroidInjectionModule;

@DataScope
@Component(
        dependencies = DataComponent.class,

        modules = {

                AndroidInjectionModule.class,

                ActivityBuilder.class,
                FragmentBuilder.class
        })
public interface AppComponent {

    void inject(HitBTCApplication application);
}
