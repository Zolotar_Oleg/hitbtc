package com.oliver.hitbtctrader.domain;


import com.oliver.hitbtctrader.domain.model.Ticker;

import io.reactivex.Observable;

public interface TickerRepository {
    Observable<Ticker> getTicker(String symbol);
}
