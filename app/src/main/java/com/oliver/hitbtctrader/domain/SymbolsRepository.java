package com.oliver.hitbtctrader.domain;


import com.oliver.hitbtctrader.domain.model.Symbol;

import java.util.List;

import io.reactivex.Flowable;

public interface SymbolsRepository {

    Flowable<List<Symbol>> getSymbolsList();
}
