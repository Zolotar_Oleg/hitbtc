package com.oliver.hitbtctrader.domain.test;


import com.oliver.hitbtctrader.data.api.model.content.TickerApiModel;
import com.oliver.hitbtctrader.domain.model.Ticker;

import java.util.Random;

public class TickerTestData {
    private final int SIZE = 5;

    private final String[] LAST = {"0.0000004077", "2499.99", "3397.25", "0.059409", "0.000000563",};
    private final String[] BID = {"0.0000004067", "1721.37", "3388.96", "0.059408", "0.000000562",};
    private final String[] ASK = {"0.0000004074", "2499.99", "3392.34", "0.059409", "0.000000563",};
    private final String[] HIGH = {"0.0000004001", "2499.99", "3232.99", "0.058032", "0.000000561",};
    private final String[] LOW = {"0.0000004366", "2499.99", "3424.36", "0.060890", "0.000000585",};
    private final String[] VOLUME = {"0.0000004227", "2499.17", "3314.92", "0.059079", "0.000000577",};
    private final String[] OPEN = {"467499100", "0.03", "1441.43", "24487.701", "204238000",};
    private final String[] VOLUME_QUOTE = {"194.0000000000", "75.00", "4807383.00", "1459.470000", "116.000000000",};
    private final Long[] TIMESTAMP = {1502366963695L, 1502366963695L, 1502366963695L, 1502366963695L, 1502366963695L};

    private Random r = new Random();


    public TickerApiModel getTickerApiModel(int index) {
        TickerApiModel result = new TickerApiModel();
        result.setLast(LAST[index]);
        result.setBid(BID[index]);
        result.setAsk(ASK[index]);
        result.setHigh(HIGH[index]);
        result.setLow(LOW[index]);
        result.setVolume(VOLUME[index]);
        result.setOpen(OPEN[index]);
        result.setVolumeQuote(VOLUME_QUOTE[index]);
        result.setTimestamp(TIMESTAMP[index]);
        return result;
    }

    public Ticker getTickerModel(int index) {
        Ticker result = new Ticker();
        result.setLast(LAST[index]);
        result.setBid(BID[index]);
        result.setAsk(ASK[index]);
        result.setHigh(HIGH[index]);
        result.setLow(LOW[index]);
        result.setVolume(VOLUME[index]);
        result.setOpen(OPEN[index]);
        result.setVolumeQuote(VOLUME_QUOTE[index]);
        result.setTimestamp(TIMESTAMP[index]);
        return result;
    }


    public Ticker getRandomTickerModel() {
        return getTickerModel(getRandomIndex());
    }

    public int getRandomIndex() {
        return r.nextInt(SIZE);
    }
}
